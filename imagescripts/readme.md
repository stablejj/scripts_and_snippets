# massconvert image resizer

## Requirements
this script depends on imagemagicks convert command.
For info about imagemagick visit:
https://www.imagemagick.org/script/index.php

## Usage
with massconvert you can resize all images inside a folder by a percentage

when executed the script prompts for a percentage to resize by and a prefix for the new filename

